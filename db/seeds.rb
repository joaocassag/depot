# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Product.delete_all

Product.create(title: 'First record',
  description:
    %{<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Metus dictum at tempor commodo ullamcorper. Faucibus et molestie ac feugiat sed lectus. Tempor nec feugiat nisl pretium fusce id. Sollicitudin nibh sit amet commodo. Placerat orci nulla pellentesque dignissim enim. Facilisis gravida neque convallis a cras semper. Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Phasellus vestibulum lorem sed risus ultricies. Gravida arcu ac tortor dignissim convallis aenean et.</p>}, image_url: 'cart.jpg', price: 26.00)

  Product.create(title: 'Second record',
    description:
      %{<p>At elementum eu facilisis sed odio. Ac turpis egestas sed tempus. Aliquam etiam erat velit scelerisque in dictum non consectetur a. Consequat ac felis donec et odio pellentesque diam. Malesuada nunc vel risus commodo. Commodo sed egestas egestas fringilla phasellus faucibus scelerisque. Gravida cum sociis natoque penatibus et. Et malesuada fames ac turpis. Non arcu risus quis varius quam quisque. Lacus suspendisse faucibus interdum posuere lorem ipsum dolor sit. Lobortis feugiat vivamus at augue eget arcu dictum. Et ligula ullamcorper malesuada proin libero nunc consequat interdum varius. Mi quis hendrerit dolor magna eget. Volutpat ac tincidunt vitae semper. Pulvinar neque laoreet suspendisse interdum. Eget arcu dictum varius duis at consectetur.</p>}, image_url: 'cart.jpg', price: 30.00)
